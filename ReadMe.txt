                -----Instructions for running without docker-----

If you want to use this without using docker.
First you'll need to download the forge installer.
Then you'll need to download the packwiz-boostrap-installer and run the command below to get the mod jars.
java -jar packwiz-installer-boostrap.jar -g -s server file:///path/to/pack.toml
that will install the mods for the server once that's done you can run the forge installer
and then run the server like normal

                -----Instructions for running using docker-----

In this directory use the command docker build -t name-of-choice .
This will take a while to build I'm using the Fedora Image it's a little bloated but I had problems building with Ubuntu or Debian for arm64.
once the package is built use the command
docker run -dit -v /host/path/of/choice:/Minecraft/world -v /path/of/choice/ops.json:/Minecraft/ops.json -v /path/of/choice/whitelist.json:/Minecraft/whitelist.json -v /path/of/choice/server.properties:/Minecraft/server.properties -v /path/of/choice/dynmap:/Minecraft/dynmap -p 25565:25565 -p 8123:8123 name-of-choice
This will keep the world, ops list, whitelist, server properites, and the web hosted map persistent. feel free to change the external ports and to add any custom bind mounts. The root of the minecraft stuff should be at /Minecraft internally on the container. The 8123 port that's opened is for dynmap it's a webpage based game map. If you don't want to use it feel free to remove -p 8123:8123 from the launch flags and it won't be accesible. For further guidance on the map check out their curseforge page. https://www.curseforge.com/minecraft/mc-mods/dynmapforge

If you ever need to run a command as the server you can use docker attach name-of-choice to run commands as the server. Then use Ctrl+p,then Ctrl+q to detach from the console without killing the server.

If you want to run commands in the container that aren't minecraft related you can use dokcer exec name-of-choice /bin/bash to run any commands on the contasiner. You can type exit to exit this without killing the server.
