FROM fedora
RUN echo Running this build file accepts the minecraft EULA
RUN dnf upgrade -y
RUN dnf install java-17-openjdk -y
RUN dnf install wget -y
COPY . /Minecraft
WORKDIR /Minecraft
RUN cd /Minecraft
RUN wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.18.2-40.1.71/forge-1.18.2-40.1.71-installer.jar
RUN wget https://github.com/packwiz/packwiz-installer-bootstrap/releases/download/v0.0.3/packwiz-installer-bootstrap.jar
RUN echo "eula=true" > eula.txt
RUN java -jar forge-*.jar --installServer
RUN java -jar packwiz-installer-bootstrap.jar -g -s server file:///Minecraft/pack.toml
RUN rm -r mods/*.toml
RUN rm index.toml pack.toml packwiz-installer-bootstrap.jar packwiz-installer.jar packwiz.json forge-*-installer.jar
RUN mkdir world
RUN mkdir dynmap
CMD ./run.sh
