# Techno Hell
Welcome to the project files for the Techno Hell Minecraft modpack.

***

## How to work on the modpack
1. Download [packwiz](https://github.com/packwiz/packwiz).
2. Set your environment variables to reference its location.

>**Example**: Set your .zshrc or .bashrc file to include this `export PATH=$PATH:/opt/packwiz`

3. Clone the project.
```
git clone git@gitlab.com:w1zz4rddr4g0n-modpacks/techno-hell.git
```
4. Make sure you're in the project directory.
```
cd techno-hell
```
5. Use packwiz to execute desired changes.

>Add a mod using this command `packwiz cf install modname`

>Update the mods using this command `pcakwiz update --all`

>Remove a mod using this command `packwiz remove modname`

>Export the modpack into a usable format by polymc using this command `packwiz cf export`

>Change the modpack version number and forge version by editing the `pack.toml` file
>or by using the `packwiz init` command